#!/bin/bash
set -x
RightNowIs=`/usr/libexec/PlistBuddy -c "Print :CurrentDateTime" /tmp/HVT.plist`
BackupDir="$HOME/Desktop/Kexts Backup/Backup $RightNowIs"
kextdir="$2/System/Library/Extensions/"
Kexts=""
case $1 in
aicpm)	Kexts=("AppleIntelCPUPowerManagement.kext")
		;;
fbsnb)	Kexts=("AppleIntelSNBGraphicsFB.kext")
		;;
fbivy)	Kexts=("AppleIntelFramebufferCapri.kext")
		;;
fbivy_mav)	Kexts=("AppleIntelFramebufferCapri.kext")
		;;
aprtc)	Kexts=("$AppleRTC.kext")
		;;
rtcmav)	Kexts=("AppleRTC.kext")
		;;
idt*|alc*)		Kexts=("AppleHDA.kext" "VoodooHDA.kext" "AppleHDADisabler.kext")
		;;
esac
	if [ ! -e "$BackupDir" ]
	then
		mkdir -p "$BackupDir"
	fi
for kext in ${Kexts[@]}; 
do
if [ -e "$kextdir$kext" ]
then
	cp -Rf "$kextdir$kext" "$BackupDir"
fi
done